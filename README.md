A REST API made with Node and Express for starting up new Godot Engine game servers, as Docker containers, on demand whenever a player wants to initiate a match.

- Adjust the model with the fields you need for your game servers.
- Adjust the parameters that are sent to the Godot server inside the container, to fit your game.
 

Made this for my game Critically Entangled: https://gitlab.com/ScyDev/Critically-Entangled